@extends('admin.layouts.admin')

@section('content')
<div class=""  id="root">
  <div class="page-title">
    <div class="title_left">
      <h3>@{{header}}</h3>
    </div>

    <div class="title_right">
      <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
        <div class="input-group">
          <input type="text" class="form-control" placeholder="搜索">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button">Go!</button>
          </span>
        </div>
      </div>
    </div>
  </div>

  <div class="clearfix"></div>

  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <sheet><isheet v-for="(list,index) in lists" :key="list.no" :index="index" :item="list"></isheet></sheet>
    </div>
  </div>
</div>
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/dashboard.js')) }}
    <script src="{{ asset('js/order/orderlist.js') }}"></script>
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
@endsection
  