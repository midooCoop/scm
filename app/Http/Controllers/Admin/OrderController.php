<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function order(Request $request){
      return view('admin.ordering.order');
    }
    public function track(Request $request){
      return view('admin.ordering.orderTrack');
    }
    public function list(Request $request){
      return view('admin.ordersAdmin.orderlist');
    }
}
