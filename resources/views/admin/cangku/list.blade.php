@extends('admin.layouts.admin')

@section('content')
    <!-- 页面内容 -->
    <div class="my-container" role="main">
      <div class="">
        <div class="page-title">
          <div class="title_left">
            <h3>添加库存</h3>
          </div>

          <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="搜索...">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                </span>
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12 pull-right">
                <button class="btn btn-primary add">
                  添加库存
                </button>
              </div>
          </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- 筛选 -->
          
            <!-- 表格 -->
            <div class="container">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>编号</th>
                    <th>品牌</th>
                    <th>商品名称</th>
                    <th>最近采购价</th>
                    <th>保质期</th>
                    <th>数量</th>                     
                    <th>备注</th>
                    <th>种类</th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="(item,index) in kucuns">
                    <td>@{{ index+1 }}</td>
                    <td>@{{item.has_one_good.brand}}</td>
                    <td>@{{item.has_one_good.name}}</td>
                    <td>@{{item.caigou_price}}</td>
                    <td>@{{item.has_one_good.baozhiqi}}</td>
                    <td>@{{ item.quantity }}</td>
                    <td>@{{item.comment}}</td>
                    <td>@{{item.has_one_good.type}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /页面内容 -->

@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/dashboard.js')) }}
    <script>
      $(function(){
        $('.add').click(function(){
          window.open('{{ url('admin/kucun/add') }}');
        });

        $('.search-btn').click(function(){
          var keyword = $('.keyword').val();
          window.location.search = $.query.set('keyword',keyword);
        });

        $.ajax({
          url : '{{ url('api/web/cangku/all') }}',
          data : {
            _token : '{{csrf_token()}}'
          },
          method : 'POST',
          dataType : 'json',
          success : function(data){
            if(data.status == 0){
              vm.kucuns = data.values.kucuns;
            }
          },
          error : function(){
            alert('error');
          }
        });


      });
 


      var vm = new Vue({
        el : '.my-container',
        data : {
          kucuns : []
        }
      });
    </script>
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
@endsection
