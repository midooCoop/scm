Vue.component('sheet', {
    template:`
    <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th class="text-center">行号</th>
        <th class="text-center">订单号</th>
        <th class="text-center">总价</th>        
        <th class="text-center">状态</th>
      </tr>
    </thead>
    <tbody>
    <slot></slot>
    </tbody>
  </table>
    `
})
Vue.component('isheet', {
    props:['item','index'],
    template:`
    <tr>
    <td class="text-center">{{index+1}}</td>
    <td class="text-center"><a href="">{{item.bianhao}}</a></td>
    <td class="text-center">{{item.sum}}元</td>
    <td class="text-center"><a :href=" '/admin/order/detail/' +item.id">
    <span v-show="(item.status=='待发货')" class="label label-info">未处理</span>
    <span v-show="(item.status=='')" class="label label-primary">处理中</span>
    <span v-show="(item.status=='')" class="label label-default">已发货</span></a>
    </td>
    </tr>
    `
})

var app = new Vue({
    el:"#root",

    data:{
        header: "订单确认",
        usrid:"测试一号",
        lists: [       
        ],

    }

})

$(function(){
  axios.post('/api/web/caigou/orderlist',{}).then(function(response){
    var data = response.data;
    if(data.status == 0){
        app.lists = data.values.orders;
    }else{
        swal(data.message);
    }
  });
});