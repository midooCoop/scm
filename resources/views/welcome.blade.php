@extends('layouts.welcome')

@section('content')
    <div class="title m-b-md">
    {{ config('app.name') }}
    </div>
    <div class="m-b-md">
        <a href="{{route('admin.dashboard')}}">进入管理页面</a>
    </div>
@endsection