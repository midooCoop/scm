<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cangku extends Model
{
    public function hasOneGood(){
      return $this->belongsTo(\App\Models\Storage\CangkuGood::class,'good_id');
    }
}
