<?php

namespace App\Models\Caigou;

use Illuminate\Database\Eloquent\Model;

class ShangjiaOrder extends Model
{
    public function item(){
      return $this->hasMany(ShangjiaOrderItem::class,'order_id');
    }
    public function user(){
      return $this->belongsTo(\App\Models\Auth\User\User::class,'user_id');
    }
}
