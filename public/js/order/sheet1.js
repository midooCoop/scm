Vue.component('sheet', {
    template:`
    <table class="table table-striped table-bordered">
    <thead>
      <tr>
      <th></th>
      <th>条码</th>
      <th>商品名称</th>
      <th>建议零售价</th>
      <th>折扣</th>
      <th>供货价</th>   
      <th>保质期</th>
      <th>备注</th>
      <th>种类</th>
      <th>数量</th> 
      </tr>
    </thead>
    <tbody>
    <slot></slot>
    </tbody>
  </table>
    `
})

Vue.component('isheet',{
  props:['item','eble'],
  template:`
    <tr>
      <td></td>
      <td>{{item.goods.sku}}</td>
      <td>{{item.goods.name}}</td>
      <td>{{item.goods.price}}</td>
      <td>{{item.goods.zhekou}}</td>
      <td>{{item.goods.gonghuojia}}</td>      
      <td>{{item.goods.baozhiqi}}</td>
      <td>{{item.goods.comment}}</td>
      <td>{{item.goods.type}}</td>
      <td>
        <input v-if="eble" type="text" v-model="item.quantity" size="3">
        <div v-else>{{item.quantity}}</div>
      </td>
    </tr>
  `
})
var app = new Vue({
    el:"#root",

    data:{
        header: "订单确认",
        usrid:"测试一号",
        usrname:"mfy",
        sid:"cg201712100001",
        tel:"1868644775",
        addr:"舒兰市新城xx街xx号",
        stat:"未处理",
        date : '',
        action:"发货",
        lists:[
        ],
        editable: false
    },
    methods:{
      edit(){
        if(!this.editable) 
        this.editable= true;
        else this.editable=false;
        },
        chuku(){
          var order_id = $('#order-id').val();
          var chuku = this.lists;
          console.log('order_id');
          axios.post('/api/web/chuku/chuku',{
            user_id : $('._user_id').val(),
            order_id : order_id,
            chuku : chuku 
          }).then(function(response){
            var data = response.data;
            if(data.status == 0){
              swal('成功');
            }else{
              swal(data.message);
            }
          });
        }
      }

});

$(function(){
  axios.post('/api/web/caigou/detail',{
    id : $('#order-id').val()
  }).then(function(response){
    var data = response.data;
    if(data.status == 0){
      var order = data.values.order;
      app.username = order.user.name;
      app.sid = order.bianhao;
      app.tel = order.user.tel;
      app.addr = order.user.address;
      app.stat = order.status;
      app.date = order.created_at;
      app.lists = data.values.order.item;
    }else{
      swal(data.message);
    }
  });
});