@extends('admin.layouts.admin')

@section('content')

          <!-- 页面内容 -->
        <div class="my-container" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>添加库存</h3>
              </div>

              <div class="title_right">
                <div class="col-md-8 col-sm-8 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" id="search"  placeholder="搜索...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                    </span>
                  </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12 pull-right">
                    <button class="btn btn-primary sure-btn">
                      确认添加
                    </button>
                  </div>
              </div>
            </div>

            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <!-- 筛选 -->
              {{--   <div class="x_panel">
                  <div class="x_title">
                    <h2>筛选</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">全选</a>
                          </li>
                          <li><a href="#">全不选</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                      <ul class="flat">
                        <li>
                          <p>
                            <input type="checkbox" class="flat"> 筛选属性 </p>
                        </li>
                        <li>
                          <p>
                            <input type="checkbox" class="flat"> 筛选属性 </p>
                        </li>
                        <li>
                          <p>
                            <input type="checkbox" class="flat"> 筛选属性 </p>
                        </li>

                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <ul class="">
                          <li>
                            <p>
                              <input type="checkbox" class="flat"> 筛选属性 </p>
                          </li>
                          <li>
                            <p>
                              <input type="checkbox" class="flat"> 筛选属性 </p>
                          </li>
                          <li>
                            <p>
                              <input type="checkbox" class="flat"> 筛选属性 </p>
                          </li>
                      </div>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                          <ul class="">
                            <li>
                              <p>
                                <input type="checkbox" class="flat"> 筛选属性 </p>
                            </li>
                            <li>
                              <p>
                                <input type="checkbox" class="flat"> 筛选属性 </p>
                            </li>

                            </li>
                        </div>
                  </div>
                </div> --}}
                <!-- 表格 -->
                <div class="container">
                  <table class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th></th>
                        <th>编号</th>
                        <th>商品编号</th>
                        <th>品牌</th>
                        <th>商品名称</th>
                        <th>单位</th>
                        <th>采购价</th>
                        <th>数量</th>
                        <th>最近供货商</th>
                        <th>备注</th>
                        <th>生产日期</th>
                        <th>仓库</th>
                        {{-- <th>种类（注：服饰，食品，etc.）</th> --}}
                      </tr>
                    </thead>
                    <tbody>
                      <tr v-for="(ruku,index) in goods">
                        <td>
                          <input type="checkbox"  v-model="ruku.is_checked">
                        </td>
                        <td>@{{index+1}}</td>
                        <td>@{{ruku.sku}}</td>
                        <td>@{{ruku.brand}}</td>
                        <td>@{{ruku.name}}</td>
                        <td>@{{ruku.unit}}</td>
                        <td><input style="width: 5rem;" type="number" v-model="ruku.caigou_price"></td>
                        <td><input style="width: 5rem;" type="number" v-model="ruku.quantity"></td> 
                        <td><input style="width: 8rem;" type="text" v-model="ruku.last_supplier"></td>
                        <td><input style="width: 7rem;" type="text" v-model="ruku.comment"></td>
                        <td><input style="width: 9rem;" type="date" name="birthdate" class="index" v-model="ruku.product_date" /></td>
                        <td><input style="width: 7rem;" type="text" v-model="ruku.house"></td>
                        {{-- <td><div class="datetimepicker"></div></td> --}}
                      </tr>
                    </tbody>
                  </table>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /页面内容 -->


@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/dashboard.js')) }}
    <script>
      $(function(){
        
        // $('#demo').daterangepicker();
        $('.import').click(function(){
          window.open('{{ url('admin/cangku/import') }}');
        });
        $('.search-btn').click(function(){
          var keyword = $('.keyword').val();
          window.location.search = $.query.set('keyword',keyword);
        });
        $('#search').change(function(){
          var keyword = $('#search').val();
          $.ajax({
            url : '{{ url('api/web/storage/all') }}',
            data : {
              keyword : keyword,
              _token : '{{csrf_token()}}'
            },
            method : 'POST',
            dataType : 'json',
            success : function(data){
              if(data.status == 0){
                vm.goods = data.values.goods;
              }
            },
            error : function(){
              alert('error');
            }
          });
        });

        $.ajax({
          url : '{{ url('api/web/storage/all') }}',
          data : {
            _token : '{{csrf_token()}}'
          },
          method : 'POST',
          dataType : 'json',
          success : function(data){
            if(data.status == 0){
              vm.goods = data.values.goods;
            }
          },
          error : function(){
            alert('error');
          }
        });

        //批量添加
        $('.sure-btn').click(function(){
          vm.added = [];
          for(var i =0 ; i<vm.goods.length; i++){
            if(vm.goods[i].is_checked){
              vm.added.push(vm.goods[i]);
            }
          }
          var data = {
            _token:'{{ csrf_token() }}',
            rukus : vm.added
          };
          $.ajax({
            url : '{{ url('api/web/cangku/add') }}',
            data : data,
            method : 'POST',
            dataType : 'json',
            success : function(data){
              if(data.status == 0){
                alert('success');
              }else{
                alert(data.message);
              }
            },
            error : function(){
              alert('error');
            }
          })
        });
      });

      // $('.datetimepicker').datetimepicker();
      var vm = new Vue({
        el : '.my-container',
        data : {
          goods : null,
          added : []
        }
      });
    </script>
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
@endsection
  