@extends('admin.layouts.admin')

@section('content')
<input type="hidden" id="order-id" value="{{$id}}">
<!-- page content -->
<div class="" id="root">
<div class="page-title">
<div class="title_left">
<h3>单据详情</h3>
</div>

<div class="title_right">
<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
<div class="input-group">
<input type="text" class="form-control" placeholder="搜索...">
<span class="input-group-btn">
<button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
</span>
</div>
</div>
</div>
</div>

<div class="clearfix"></div>

<div class="row">
<div class="col-md-12 col-sm-12 col-xs-12">
<div class="x_panel">
<div class="x_title">
<h2>订单@{{sid}}</h2>
<ul class="nav navbar-right panel_toolbox">
<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a >
</li>
<li class="dropdown">
<a href=" " class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a >
<ul class="dropdown-menu" role="menu">
<li><a href="#">设置 1</a >
</li>
<li><a href="#">设置 2</a >
</li>
</ul>
</li>
<li><a class="close-link"><i class="fa fa-close"></i></a >
</li>
</ul>
<div class="clearfix"></div>
</div>
<div class="x_content">
<div class="print">
<!-- 表单头部 -->
<div class="orderInfo">
<h1 class="text-center">美域高商贸出库清单</h1>
<div class="row text-center">
<div class="col-md-3">客户名称：@{{usrid}}</div>
<div class="col-md-3">录单日期：@{{date}}</div>
<div class="col-md-3">单号：@{{sid}}</div>
</div>
<div class="row text-center">
<div class="col-md-3">联系人：@{{usrname}}</div>
<div class="col-md-3">联系方式：@{{tel}}</div>
<div class="col-md-3">状态：@{{stat}}</div>
</div>
<div class="row text-center">
<div class="col-md-3">联系地址：@{{addr}}</div>
<div class="col-md-3 pull-right">出库类型：@{{action}}</div> 
</div>
</div>
<!-- 表单内容 -->
<sheet><isheet v-for="list in lists" :eble="editable" :key="list.id" :item="list"></isheet></sheet>
<div class="row">
<button v-show="editable" class="btn pull-right btn-primary" @click="edit">确认</button>
<button v-show="editable" class="btn pull-right btn-primary" @click="edit">取消</button>
<button v-show="!editable" class="btn pull-right btn-primary" @click="edit">编辑</button> 
<button v-show="!editable" class="btn pull-right btn-primary" @click="chuku">出库</button>
<button v-show="!editable" class="btn pull-right btn-primary" >返回列表</button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/dashboard.js')) }}
    <script src="{{ asset('js/order/sheet1.js') }}"></script>
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
@endsection
  