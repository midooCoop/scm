<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{ route('admin.dashboard') }}" class="site_title">
                <i class="fa fa-heart-o"></i>
                <span>{{ config('app.name') }}</span>
            </a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_pic">
                <img src="{{ auth()->user()->avatar }}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <h2>{{ auth()->user()->name }}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br/>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <!-- 首页 -->
            <div class="menu_section">
                <h3>{{ __('views.backend.section.navigation.sub_header_0') }}</h3>
                <ul class="nav side-menu">
                    <li>
                        <a href="{{ route('admin.dashboard') }}">
                            <i class="fa fa-home" aria-hidden="true"></i>
                            {{ __('views.backend.section.navigation.menu_0_1') }}
                        </a>
                    </li>
                </ul>
            </div>

            @if(checkAdmin())
            <!-- 仓库管理 -->
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li>
                        <a>
                            <i class="fa fa-edit"></i>
                            {{ __('views.backend.section.navigation.sub_header_1') }}
                            <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li>
                                <a href="{{ '/admin/cangku/manage' }}">
                                    {{ __('views.backend.section.navigation.menu_1_1') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('admin/kucun/list') }}">
                                    {{ __('views.backend.section.navigation.menu_1_2') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('admin/kucun/sell') }}">
                                    {{ __('views.backend.section.navigation.menu_1_3') }}
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            @endif
            @if(checkAdmin())
            <!-- 采购 -->
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li>
                        <a>
                            <i class="fa fa-truck"></i>
                            {{ __('views.backend.section.navigation.sub_header_2') }}
                            <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li>
                                <a href="{{ '/admin/order' }}">
                                    {{ __('views.backend.section.navigation.menu_2_1') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ '/admin/cangku/manage' }}">
                                    {{ __('views.backend.section.navigation.menu_2_2') }}
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            @endif
            @if(checkAdmin())
            <!-- 采购单管理 --> 
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li>
                        <a>
                            <i class="fa fa-truck"></i>
                            {{ __('views.backend.section.navigation.sub_header_2_1') }}
                            <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li>
                                <a href="{{ '/admin/order/list' }}">
                                    {{ __('views.backend.section.navigation.menu_2_1-1') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ '/admin/order/track' }}">
                                    {{ __('views.backend.section.navigation.menu_2_2-1') }}
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            @endif
            @if(checkAdmin())
            <!-- 客户管理 -->
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li>
                        <a>
                            <i class="fa fa-sitemap"></i>
                            {{ __('views.backend.section.navigation.sub_header_3') }}
                            <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li>
                                <a href="{{ route('admin.users') }}">
                                    {{ __('views.backend.section.navigation.menu_3_1') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.permissions') }}">
                                    {{ __('views.backend.section.navigation.menu_3_2') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ '/admin/' }}">
                                    {{ __('views.backend.section.navigation.menu_3_3') }}
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            @endif
            <!-- 销售报表 -->
            {{-- <div class="menu_section">
                <ul class="nav side-menu">
                    <li>
                        <a>
                            <i class="fa fa-table"></i>
                            {{ __('views.backend.section.navigation.sub_header_4') }}
                            <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li>
                                <a href="{{ '/admin/cangku/manage' }}">
                                    {{ __('views.backend.section.navigation.menu_4_1') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ '/admin/cangku/manage' }}">
                                    {{ __('views.backend.section.navigation.menu_4_2') }}
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- 数据统计 -->
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li>
                        <a>
                            <i class="fa fa-bar-chart-o"></i>
                            {{ __('views.backend.section.navigation.sub_header_5') }}
                            <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li>
                                <a href="{{ '/admin/cangku/manage' }}">
                                    {{ __('views.backend.section.navigation.menu_5_1') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ '/admin/cangku/manage' }}">
                                    {{ __('views.backend.section.navigation.menu_5_2') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ '/admin/cangku/manage' }}">
                                    {{ __('views.backend.section.navigation.menu_5_3') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ '/admin/cangku/manage' }}">
                                    {{ __('views.backend.section.navigation.menu_5_4') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ '/admin/cangku/manage' }}">
                                    {{ __('views.backend.section.navigation.menu_5_5') }}
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div> --}}
            <!-- 通信 -->
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li>
                        <a>
                            <i class="fa fa-envelope"></i>
                            {{ __('views.backend.section.navigation.sub_header_6') }}
                            <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="nav child_menu">
                            <li>
                                <a href="{{ '/admin/cangku/manage' }}">
                                    {{ __('views.backend.section.navigation.menu_6_1') }}
                                </a>
                            </li>
                            <li>
                                <a href="{{ '/admin/cangku/manage' }}">
                                    {{ __('views.backend.section.navigation.menu_6_2') }}
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- <<<<<<<br>>>>>>>> -->
            {{-- <div class="menu_section">
                <h3>{{ __('views.backend.section.navigation.sub_header_7') }}</h3>
                <ul class="nav side-menu">
                  <li>
                      <a href="midoolife.com"><i class="fa fa-lock" aria-hidden="true"></i>官网</a>
                  </li>
                </ul>
            </div> --}}
        </div>
        <!-- /sidebar menu -->
        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="选项">
            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="全屏">
            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="主题">
            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="登出" href="#">
            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
