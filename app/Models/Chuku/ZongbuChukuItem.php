<?php

namespace App\Models\Chuku;

use Illuminate\Database\Eloquent\Model;

class ZongbuChukuItem extends Model
{
    public function chuku(){
      return $this->belongsTo(Chuku::class,'chuku_id');
    }
}
