<?php

namespace App\Models\Caigou;

use Illuminate\Database\Eloquent\Model;

class ShangjiaOrderItem extends Model
{
    public function order(){
      return $this->belongsTo(ShangjiaOrder::class,'order_id');
    }
    public function goods(){
      return $this->belongsTo(\App\Models\Storage\CangkuGood::class,'goods_id');
    }
}
