@extends('admin.layouts.admin')

@section('content')
<!-- 页面内容 -->
  <div class="my-container">
  <div class="page-title">
          <div class="title_left">
            <h3>添加商品</h3>
          </div>

          <div class="title_right">
            <div class="col-md-4 col-sm-4 col-xs-12 form-group pull-right top_search">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="搜索...">
                <span class="input-group-btn">
                  <button class="btn btn-default search-btn" type="button"><i class="fa fa-search"></i></button>
                </span>
              </div>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12 pull-right">
                <span class="btn btn-primary" data-toggle="modal" data-target="#import">
                批量导入
                </span>
            </div>
            <div v-show="is_show" class="col-md-2 col-sm-2 col-xs-12 pull-right">
              <span  class="btn btn-primary delete">
                确认删除
              </span>
            </div>
            <div v-show="is_show" class="col-md-2 col-sm-2 col-xs-12 pull-left">
              <span  class="btn btn-primary edit">
                确认编辑
              </span>
            </div>
            <div v-show="is_show" class="col-md-2 col-sm-2 col-xs-12 pull-left">
              <span  class="btn btn-primary all" v-show="can_select_all">
                全选
              </span>
              <span  class="btn btn-primary cancel" v-show="!can_select_all">
                取消全选
              </span>
            </div>
            <div v-show="!is_show" class="col-md-2 col-sm-2 col-xs-12 pull-right">
              <span  class="btn btn-primary" onclick="vm.is_show =true;">
                &nbsp;&nbsp;多选&nbsp;&nbsp;
              </span>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
    <!-- 表格 -->
    <div class="row">
      <table class="table table-striped table-bordered">
        <thead>
          <tr>
            <th></th>
            <th>编号</th>
            <th>品牌</th>
            <th>名称</th>
            <th>单位</th>
            <th>零售价</th>
            <th>保质期</th>
            <th>活动</th>
            <th>供货价</th>
            <th>折扣</th>
            <th>新品</th>
            <th>种类</th>
          </tr>
        </thead>
        <tbody>
          @foreach($goods as $key => $good)
          <tr>
            <td>{{ $key+1 }} <input v-if="is_show" type="checkbox" class="del" id="{{ $good->id }}"> </td>
            <td>{{ $good->sku }}</td>
            <td>{{ $good->brand }}</td>
            <td>{{ $good->name }}</td>
            <td>{{ $good->unit }}</td>
            <td>{{ $good->price }}</td>
            <td>{{ $good->baozhiqi }}</td>
            <td><input type="text" class="huodong" id="{{$good->id}}" value="{{ $good->huodong }}" size="8"></td>
            <td><input type="text" class="gonghuojia" id="{{$good->id}}" value="{{ $good->gonghuojia }}" size="4"></td>
            <td><input type="text" class="zhekou" id="{{$good->id}}" value="{{ $good->zhekou }}" size="4"></td>
            <td><input type="checkbox" class="is_new" id="{{$good->id}}" @if($good->is_new) checked="" @endif  ></td>
            <td>{{ $good->type }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <div class="col-xs-12 col-lg-offset-7">
      {{ $goods->links() }}
    </div>
    
  </div>
    <!-- 模态框（Modal） -->
  <div class="modal fade" id="import" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
  &times;
  </button>
  <h4 class="modal-title" id="myModalLabel">
  </div>
  <div class="modal-body">
  <form role="form" id="form-import" enctype="multipart/form-data" action="/api/web/storage/import" method="post" class="form-control">
  <div class="form-group">
  {{ csrf_field() }}
  <input type="file" name="csv" id="inputfile" class="csv">
  <p class="help-block">导入需要固定表头的csv格式文件</p >
  </div> 
  </form>
  </div>
  <div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">关闭
  </button>
  <button type="button" class="btn btn-primary sure-btn">
  提交
  </button>
  </div>
  </div><!-- /.modal-content -->
  </div><!-- /.modal -->
</div>

@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/dashboard.js')) }}
    <script>
      $(function(){
        $('.import').click(function(){
          window.open('{{ url('admin/cangku/import') }}');
        });

        $('.all').click(function(){
          $('input:checkbox').each(function(index,item){
            $(item).attr('checked',true);
          });
          vm.can_select_all = false;
        });
        $('.cancel').click(function(){
          $('input:checkbox').each(function(index,item){
            $(item).attr('checked',false);
          });
          vm.can_select_all = true;
        });
        $('.delete').click(function(){
          var delArr = [];
          $('input:checked').each(function(index,item){
            delArr.push($(item).attr('id'));
          });
         $.ajax({
          data : {delArr : delArr},
          dataType : 'json',
          method : 'POST',
          url : '{{ url('/api/web/storage/delete') }}',
          success : function(data){
            if(data.status == 0){
              swal('成功');
              window.location.reload();
            }
          },
          error : function(data){

          }
         });
        });

        $('.edit').click(function(){
          var editArr = [];
          $('input:checked').each(function(index,item){
            var is_new = $( $($(item).parent()).parent().find('.is_new')).is(':checked') ? 1 : 0;
            editArr.push({
              id : $(item).attr('id'),
              huodong : $( $($(item).parent()).parent().find('.huodong')).val(),
              zhekou :  $( $($(item).parent()).parent().find('.zhekou')).val(),
              gonghuojia :  $( $($(item).parent()).parent().find('.gonghuojia')).val(),
              is_new :  is_new
            });
          });
         $.ajax({
          data : {editArr : editArr},
          dataType : 'json',
          method : 'POST',
          url : '{{ url('/api/web/storage/edit') }}',
          success : function(data){
            if(data.status == 0){
              swal('成功');
              window.location.reload();
            }
          },
          error : function(data){

          }
         });
        });
        $('.sure-btn').click(function(){
          $('#form-import').submit();
          return false;
        });

        @if(isset($success) && $success == 1)
          swal('导入成功');
        @endif

        $('.search-btn').click(function(){
          var keyword = $('.keyword').val();
          window.location.search = $.query.set('keyword',keyword);
        });
      });

      var vm = new Vue({
        el : '.my-container',
        data : {
          is_show : false,
          can_select_all : true
        }
      });
    </script>
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
@endsection
