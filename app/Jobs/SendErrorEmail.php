<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendErrorEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $e;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($e)
    {
        $this->e = $e;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ( !in_array(get_class($this->e), $this->dontReport) && env('APP_ENV', 'local') == 'server' ) {
        $errorMsg = $this->e->getFile().$this->e->getLine();
        if ( true ) {
          $data = [
            'destinations' => [
                ['fanyi-meng@outlook.com','孟繁熠'],
                ['mrzhangrb@outlook.com','张若彬']
              ],
            'content' => "<h3>出现错误：".$this->e->getMessage()."<h3>".$this->e->getFile()."\n".$this->e->getLine().' 行'
          ];
          \Mail::send('mail.bug', $data, function($message) use($data)
          {
            foreach ($data['destinations'] as $destination) {
              $message->to($destination[0], $destination[1])->subject('error report');
            }
          });
        }
      }
    }
}
