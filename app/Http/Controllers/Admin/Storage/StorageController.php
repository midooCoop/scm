<?php

namespace App\Http\Controllers\Admin\Storage;

use App\Http\Controllers\Controller;
use App\Models\Storage\CangkuGood;
use Illuminate\Http\Request;
class StorageController extends Controller{
  
  public function list(Request $request){
    $keyword = $request->get('keyword','');
    $keywords = explode(' ', $keyword);
    $goods = CangkuGood::where('id','>',0)->orderBy('id','desc');
    if(empty($keywords)){
        $goods = $goods->paginate(20)->appends(['keyword'=>$keyword]);
        // $goods = $goods->orderBy('id','desc')->get();
      }else{
        $num = count($keywords);
        if($num == 1){
          foreach($keywords as $k){
            $goods->where('brand','like',"%$k%")
                  ->orWhere('sku','like',"%$k%")
                  ->orWhere('name','like',"%$k%");
          }
        }else{
          $goods->where('brand','like',"%".$keywords[0]."%")
                ->where('name','like',"%".$keywords[1]."%");
        }
        $goods = $goods->paginate(20)->appends(['keyword'=>$keyword]);
      }
    return view('admin.storage.manage')->withGoods($goods)
                                       ->withKeywords($keyword);
  }

  public function import(Request $request){
    return view('admin.storage.import');
  }
}