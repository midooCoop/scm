@extends('admin.layouts.admin')

@section('content')

<div id="root">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>@{{header}}</h3><h4>欢迎，@{{usrid}}</h4>
      </div>

      <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="搜索...">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
            </span>
          </div>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12 pull-right">
            <button class="btn btn-primary" data-toggle="modal" data-target="#import">
              购物车
            </button>
          </div>
        <!-- 模态框（Modal） -->
          <cart :usrid="usrid" @submit="submit_order"><caigou v-on:remove="remove(index)" v-for="(list,index) in cargo" :index="index" :item="list"></caigou></cart>
        </div>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="container">
          <tcaigou><icaigou  v-for="list in lists" v-on:add="additem" :key="list.id" :item="list"></icaigou></tcaigou>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/dashboard.js')) }}
    <script src="{{ asset('js/order/dinghuo.js') }}"></script>
@endsection

@section('styles')
    @parent
    {{ Html::style(mix('assets/admin/css/dashboard.css')) }}
@endsection
  