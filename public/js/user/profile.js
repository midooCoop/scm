Vue.component('ctr-item', {
    props: ['item', 'texture'],
    template : `
    <div class="row">
    <div class="col-md-3">&nbsp&nbsp</div>
    <div class="col-md-3"><h2>{{item.title}}</h2></div>
    <div class="col-md-4"  v-if="texture">
    <input type="text"v-model='item.val' @keyUp.enter="edit" >
    </div>
    <div class="col-md-4"  v-else>
    <h2>{{item.val}}</h2></div>
    </div>
    `

})
var app = new Vue ({
    el: '#app',

    data:{
        ctrs:[
            {title:"姓名",val:"" },
            {title:"联系方式",val:"" },
            {title:"密码",val:"" },
            {title:"地址",val:"   " }
        ],

        texture: false
    },

    methods:{
        edit(){
            if(!this.texture) {
                this.texture= true;
            }else {
                axios.post('/api/web/user/edit',{
                    id:$('#user-id').val(),
                    tel:this.ctrs[1].val,
                    name:this.ctrs[0].val,
                    addr:this.ctrs[3].val,
                }).then(function(response){
                    if(response.data.status == 0){
                        swal('成功');
                    }
                });
                this.texture=false;
            };
        },

    }
})