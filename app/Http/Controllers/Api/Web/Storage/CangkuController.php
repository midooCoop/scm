<?php

namespace App\Http\Controllers\Api\Web\Storage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cangku,App\Models\CangkuBatch;

class CangkuController extends Controller
{
    public function ruku(Request $request)
    {
      $rukus = $request->get('rukus');
      foreach($rukus as $ruku){
        $cangku = Cangku::where('sku',$ruku['sku'])->first();
        if($cangku){
          $cangku->quantity = $cangku->quantity + $ruku['quantity'];
          if(isset($ruku['last_supplier'])){
            $cangku->last_supplier = $ruku['last_supplier'];
          }
          $cangku->chuku_price = $ruku['price'];
          $cangku->comment = $ruku['comment'];
          $cangku->caigou_price = $ruku['caigou_price']; //最近采购价
        }else{
          $cangku = new Cangku;
          $cangku->sku = $ruku['sku'];
          $cangku->good_id = $ruku['id'];
          $cangku->quantity = $ruku['quantity'];
          $cangku->comment = $ruku['comment'];
          if(isset($ruku['last_supplier'])){
            $cangku->last_supplier = $ruku['last_supplier'];
          }else{
            return $this->apiResponse(1,'初次添加,请输入供应商名字');
          }
          $cangku->caigou_price = $ruku['caigou_price'];
        }
        $batch = new CangkuBatch;
        $batch->sku = $ruku['sku'];
        $batch->good_id = $ruku['id'];
        $batch->quantity = $ruku['quantity'];
        $batch->comment = $ruku['comment'];
        $batch->house = $ruku['house'];
        if(isset($ruku['last_supplier'])){
          $batch->last_supplier = $ruku['last_supplier'];
        }else{
          $batch->last_supplier = $cangku->last_supplier;
        }
        $batch->caigou_price = $ruku['caigou_price'];
        $batch->expire_date =  \Carbon\Carbon::parse($ruku['product_date'])->addMonth($ruku['baozhiqi']);
        
        $batch->save();
        $cangku->save();
      }
      return $this->apiResponse(0,'success');
    }

    public function all(){
      $kucuns = Cangku::with('hasOneGood')->withCount(['hasOneGood as order_quantity'=>function($query){
        $query->where('id',0);
      }])->get();
      return $this->apiResponse(0,'success',['kucuns'=>$kucuns]);
    }
}
