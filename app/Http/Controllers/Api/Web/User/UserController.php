<?php

namespace App\Http\Controllers\Api\Web\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Auth\User\User;
class UserController extends Controller
{
    public function getUserInfo(Request $request){
      return $this->apiResponse(0,'success',User::find($request->get('id',0)));
    }
    public function edit(Request $request){
      $user = User::find($request->get('id',0));
      if($user == null){
        return $this->apiResponse(1,'error');
      }
      $user->name = $request->get('name');
      $user->tel = $request->get('tel');
      $user->address = $request->get('addr');
      $user->save();
      return $this->apiResponse(0,'success');
    }
}
