<?php

namespace App\Http\Controllers\Api\Web\Caigou;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Caigou\ShangjiaOrder,App\Models\Caigou\ShangjiaOrderItem;
class CaigouController extends Controller
{
    public function order(Request $request)
    {
      $orders = $request->get('orders',[]);
      $user_id = $request->get('id',0);
      if(count($orders) == 0){
        return $this->apiResponse(1,'没有采购货物');
      }

      $order = new ShangjiaOrder;
      $count = ShangjiaOrder::where('created_at','like',date('Y-m-d').'%')->count();
      $order->bianhao = 'CG-'.date('Ymd').str_pad($count+1, 4);
      $order->status = '待发货';
      $order->is_complete = 0;
      $order->user_id = $user_id;
      $order->save();
      foreach($orders as $item){
        $order_item = new ShangjiaOrderItem;
        $order_item->goods_id = $item['good_id'];
        $order_item->order_id = $order['id'];
        $order_item->quantity = $item['quantity'];
        $order_item->is_complete = 0;
        $order_item->save();
      }
      return $this->apiResponse(0,'success',['order_id'=>$order->id]);
    }

    public function orderlist(Request $request){
      $orders = ShangjiaOrder::all();
      return $this->apiResponse(0,'success',['orders'=>$orders]);
    }
    public function detail(Request $request){
      $id = $request->get('id',0);
      $order = ShangjiaOrder::with('user')->with(['item'=>function($query){
        $query->with('goods');
      }])->where('id',$id)->first();
      return $this->apiResponse(0,'success',['order'=>$order]);
    }
}
