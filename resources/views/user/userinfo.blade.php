@extends('admin.layouts.admin')
@section('content')
<div class="" id="app">
  <div class="page-title">
    <div class="title_left">
      <h3>个人信息</h3>
    </div>
  </div>
  <div class="clearfix"></div>
    <div class="text-center">
      <div class="row">
        <img src="" class="img-circle">
      </div>

      <br>

      <div class="row">
        <button v-if="!texture" class="btn btn-primary" @click="edit">编辑</button>
        <button v-else class="btn btn-primary" @click="edit">确认</button>                  
      </div>
      <ctr-item  v-for="item in ctrs" :item="item"  :texture="texture"></ctr-item>
</div>
<input type="hidden" id='user-id' value="{{ auth()->user()->id }}">
@endsection

@section('scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/dashboard.js')) }}
    <script src="{{ asset('js/user/profile.js') }}"></script>
    <script>
      $(function(){
        axios.post('{{ url('api/web/user/fetch') }}',{
          _token : '{{ csrf_token() }}',
          id : {{auth()->user()->id}}
        }).then(function(data){
          console.log(data.data.values);
          if(data.data.status == 0){
            var user = data.data.values;
            app.ctrs[0].val = user.name;
            app.ctrs[1].val = user.tel;
            app.ctrs[2].val = user.tel;
            app.ctrs[3].val = user.address;
          }
        });
      });
      
    </script>
@endsection