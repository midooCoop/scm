<?php

namespace App\Http\Controllers\Api\Web\Chuku;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Caigou\ShangjiaOrder,App\Models\Chuku\ZongbuChuku,App\Models\Chuku\ZongbuChukuItem;
class ChukuController extends Controller
{
    public function chuku(Request $request)
    {
      $order_id = $request->get('order_id',0);
      $chukus = $request->get('chuku',[]);
      if(count($chukus) == 0){
        return $this->apiResponse(1,'没有出库物品');
      }
      if($order_id == 0){
        return $this->apiResponse(1,'没有这个订单');
      }
      $order = ShangjiaOrder::with('item')->where('id',$order_id)->first();
      if(!$order){
        return $this->apiResponse(1,'没有这个订单');
      }
      if($order->is_complete == 1){
        return $this->apiResponse(1,'这个订单发货完毕');
      }
      $chuku = new ZongbuChuku;
      $count = ZongbuChuku::where('created_at','like',date('Y-m-d').'%')->count();
      $chuku->bianhao = 'CK-'.date('Ymd').($count+1);
      $chuku->user_id = $request->get('user_id');
      $chuku->order_id = $order->id;
      $chuku->save();
      foreach($chukus as $item){
        // return $item;
        $order_item = $order->item()->where('goods_id',$item['goods']['id'])->get()->first();
        $chuku_item = new ZongbuChukuItem;
        $chuku_item->chuku_id = $chuku->id;
        $chuku_item->goods_id = $item['id'];
        $chuku_item->quantity = $item['quantity'];
        $chuku_item->save();
        $chukuItems = ZongbuChukuItem::where('chuku_id',$chuku->id)->where('goods_id',$item['id'])->get();
        $count = 0;
        foreach($chukuItems as $citem){
          $count += $citem->quantity;
        }
        if($count >= $order_item->quantity){
          $order_item->is_complete = 1;
        }
        $order_item->quantity = $order_item->quantity - $chuku_item->quantity > 0 ? $order_item->quantity - $chuku_item->quantity : 0;
        $order_item->save();
      }
      $is_complete = 1;
      foreach($order->item as $item){
        if($item->is_complete == 0){
          $is_complete = 0;
          break;
        }
      }
      if($is_complete == 1){
        $order->is_complete = 1;
        $order->save();
      }
      return $this->apiResponse(0,'success');
    }
}
