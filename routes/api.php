<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['namespace'=>'Api'],function(){
  Route::group(['namespace'=>'Web','prefix'=>'web'],function(){
    Route::group(['namespace'=>'Storage','prefix'=>'storage'],function(){
      Route::post('import','StorageController@import');
      Route::post('delete','StorageController@delete');
      Route::post('edit','StorageController@edit');
      Route::post('all','StorageController@all');
    });
    Route::group(['namespace'=>'Storage','prefix'=>'cangku'],function(){
      Route::post('add','CangkuController@ruku');
      Route::post('all','CangkuController@all');
    });
    Route::group(['prefix'=>'user','namespace'=>'User'],function(){
      Route::post('fetch','UserController@getUserInfo');
      Route::post('edit','UserController@edit');
    });
    Route::group(['prefix'=>'caigou','namespace'=>'Caigou'],function(){
      Route::post('/','CaigouController@order');
      Route::post('/orderlist','CaigouController@orderlist');
      Route::post('/detail','CaigouController@detail');
    });
    Route::group(['prefix'=>'chuku','namespace'=>'Chuku'],function(){
      Route::post('/chuku','ChukuController@chuku');
    });
  });
});
