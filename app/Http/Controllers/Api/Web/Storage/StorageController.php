<?php

namespace App\Http\Controllers\Api\Web\Storage;

use Illuminate\Http\Request;
use App\Models\Storage\CangkuGood;
use App\Http\Controllers\Controller;

class StorageController extends Controller
{
    public function import(Request $request){
      $file = $request->file('csv');
      $entension =  $file -> getClientOriginalExtension();
      $tabl_name = date('YmdHis').mt_rand(100,999);
      $newName = $tabl_name.'.'.'csv';
      $path = $file->move(public_path().'/upload/storage/',$newName);
      $cretae_path = public_path().'/upload/storage/'.$newName;

      \Excel::load($cretae_path,function($reader){
        $reader = $reader->getSheet(0);
        $data = $reader->toArray();
        $column = $data[1];
        array_shift($data);
        array_shift($data);
        foreach($data as $row){
          \DB::transaction(function()use($column,$row){
            $good = new CangkuGood;
            foreach($column as $key => $col){
              $good->$col = $row[$key];
            }
            $good->save();
          });           
        }
      });
      return back();
    }

    public function all(Request $request){
      $goods = CangkuGood::where('id','>',0);
      $keywords = explode(' ', $request->get('keyword'));
      if(empty($keywords)){
        $goods = $goods->orderBy('id','desc')->get();
      }else{

        $num = count($keywords);
        if($num == 1){
          foreach($keywords as $k){
            $goods->where('brand','like',"%$k%")
                  ->orWhere('sku','like',"%$k%")
                  ->orWhere('name','like',"%$k%");
          }
        }else{
          $goods->where('brand','like',"%".$keywords[0]."%")
                ->where('name','like',"%".$keywords[1]."%");
        }
        $goods=$goods->get();
      }
      foreach($goods as $key => $good){
        $goods[$key]->is_checked = false;
        $goods[$key]->quantity = 0;
        $goods[$key]->caigou_price = 0;
        $goods[$key]->product_date = '2001-01-01'; 
        $goods[$key]->caigou_price = '';      
        $goods[$key]->house = '';      
      }
      return $this->apiResponse(0,'success',['goods'=>$goods]);
    }

    public function delete(Request $request){
      $arr = $request->get('delArr',[]);
      if(count($arr) == 0){
        return $this->apiResponse(1,'error');
      }
      CangkuGood::whereIn('id',$arr)->delete();
      return $this->apiResponse(0,'success');
    }

    public function edit(Request $request){
      $arr = $request->get('editArr',[]);
      if(count($arr) == 0){
        return $this->apiResponse(1,'error');
      }
      // return $arr;
      foreach($arr as $goods){
        $cangku = CangkuGood::where('id',$goods['id'])->first();
        $cangku->huodong = $goods['huodong'];
        $cangku->zhekou = $goods['zhekou'];
        if(isset($goods['gonghuojia'])){
          $cangku->gonghuojia = $goods['gonghuojia'];
        }else{
          $cangku->gonghuojia = $goods['zhekou'] * $cangku->price;
        }
        $cangku->is_new = $goods['is_new'];
        $cangku->save();
      }
      return $this->apiResponse(0,'success');
    }
}
