<?php

namespace App\Http\Controllers\Admin\Good;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GoodController extends Controller
{
    public function add(Request $request){
      return view('admin.cangku.add');
    }
    public function list(Request $request){
      return view('admin.cangku.list');
    }
    public function sell(Request $request){
      return view('admin.cangku.sell');
    }
}
