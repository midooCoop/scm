Vue.component('tcaigou', {
  template:`
  <table class="table table-striped table-hover">
  <thead>
    <tr>
      <th></th>
      <th>行号</th>
      <th>条码</th>
      <th>商品名称</th>
      <th>建议零售价</th>
      <th>折扣</th>
      <th>供货价</th>   
      <th>保质期</th>
      <th>备注</th>
      <th>种类</th>
      <th>库存</th>      
      <th>数量</th>      
      <th></th>
    </tr>
  </thead>
  <tbody>
  <slot></slot>
  </tbody>
</table>
  `
})
Vue.component('icaigou', {
  props:['item'],
  template:`
    <tr>
      <td><input type="checkbox" class="" v-model="item.is_select" ></td>
      <td>{{item.id}}</td>
      <td>{{item.sku}}</td>
      <td>{{item.has_one_good.name}}
        <span v-show="(item.has_one_good.is_new>0)" class="label label-success">新品</span>
        <span v-show="item.has_one_good.huodong" class="label label-danger">活动</span>
      </td>
      <td>{{item.has_one_good.price}}</td>
      <td>{{item.has_one_good.zhekou}}</td>
      <td>{{item.has_one_good.gonghuojia}}</td>      
      <td>{{item.has_one_good.baozhiqi}}</td>
      <td>{{item.has_one_good.notes}}</td>
      <td>{{item.has_one_good.type}}</td>
      <td>{{item.quantity}}</td>
      <td><input type="text" v-model='item.order_quantity' size="4"></td>
      <td><button class="btn btn-prmary btn-xs" v-on:click="add(item)">添加</button></td>
    </tr>
  `,
  methods : {
    add : function(item){
      this.$emit('add',item);
    }
  }
})
Vue.component('caigou', {
  props:['item','index'],
  template:`
    <tr>
      <td><input type="checkbox" class="" v-model="item.is_select"></td>
      <td>{{item.sku}}</td>
      <td>{{item.has_one_good.name}}
      <span v-show="(item.is_new>0)" class="label label-warning">新品</span>
      <span v-show="item.has_one_good.huodong" class="label label-success">活动</span>
      </td>
      <td>{{item.has_one_good.price}}</td>
      <td>{{item.has_one_good.zhekou}}</td>
      <td>{{item.has_one_good.gonghuojia}}</td>            
      <td>{{item.has_one_good.comment}}</td>
      <td>{{item.quantity}}</td>
      <td><input type="text" size="3" v-model='item.quantity'></td>
      <td><button class="btn btn-prmary btn-xs" v-on:click="remove(index)">删除</button></td>
    </tr>
  `,
  methods : {
    remove(index){
      this.$emit('remove',index);
    }
  }
})
Vue.component('cart',{
  props:['usrid'],
  template:`
  <div class="modal fade" id="import" tabindex="-1" role="page" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          &times;
        </button>
        <h4 class="modal-title" id="myModalLabel">{{usrid}}</h4>
      </div>
      <div class="modal-body">
      <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th></th>
          <th>条码</th>
          <th>商品名称</th>
          <th>建议零售价</th>
          <th>折扣</th>
          <th>供货价</th>          
          <th>备注</th>
          <th>数量</th>      
          <th></th>
        </tr>
      </thead>
      <tbody>
      <slot></slot>
      </tbody>
    </table>     
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭
        </button>
        <button type="button" class="btn btn-primary" v-on:click="submit">
          确认提交
        </button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal -->
  `,
  methods:{
    submit(){
      this.$emit('submit');
    }
  }
})

var app = new Vue({
  el:"#root",

  data:{
      header: "订货(此处根据路由改变)",
      usrid:"测试一号",
      cargo: [
      ],
      lists:[
        ]
    },
  computed:{

  },
  methods:{
    additem(item){
      this.cargo.push(item);
    },
    remove(index){
      this.cargo.splice(index,1);
    },
    submit_order(){
      var user_id = $('._user_id').val();
      axios.post('/api/web/caigou',{'id':user_id,'orders':this.cargo}).then(function(response){
        var data = response.data;
        if(data.status == 0){
          swal('成功');
        }else{
          swal(data.message);
        }
      });
    }
  }
});

$(function(){
  axios.post('/api/web/cangku/all',{

  }).then(function(response){
    if(response.data.status == 0){
      app.lists = response.data.values.kucuns;
    }
  });

});
