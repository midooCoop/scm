<?php

namespace App\Models\Chuku;

use Illuminate\Database\Eloquent\Model;

class ZongbuChuku extends Model
{
    public function item(){
      return $this->hasMany(ZongbuChukuItem::class,'chuku_id');
    }
    public function order(){
      return $this->belongsTo(\App\Models\Caigou\ShangjiaOrder::class,'order_id');
    }
}
